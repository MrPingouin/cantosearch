# Cantosearch

Humble script providing fast queries to several online cantonese dictionaries.

```
# python3 cantosearch.py -j jan4 

人	jan4	person; human being
仁	jan4	benevolence; compassionate; kernel
儿	jan4	person; KangXi radical 10
夤	jan4	deep; attach to
亻	jan4	person (as radical left of others)
寅	jan4	third Earthly Branch


# python3 cantosearch.py -c 人

http://www.cantonese.sheik.co.uk/dictionary/characters/93/
[1] person; people; human being
[2] everybody; each; all
[3] grown-up; adult; manhood
[4] persons of a certain kind
[5] a person's character, reputation, etc
[6] a person's physical or mental condition
[7] manpower; staff; human resources
Stroke count: 2
Classifier(s): 個,班,群

```

## Supported dictionaries

For now, only http://www.cantonese.sheik.co.uk is supported.

### CantoDict
- jyutping search
- chinese character search

