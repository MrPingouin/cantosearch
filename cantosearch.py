import re
import sys
import getopt
import requests
from bs4 import BeautifulSoup
from xml.etree import ElementTree as ET

CANTODICT_CHINESE_WORD_SEARCH = "1"
CANTODICT_CHINESE_CHAR_SEARCH = "2"
CANTODICT_ENGLISH_SEARCH = "4"
CANTODICT_JYUTPING_SEARCH = "3"
CANTODICT_SEARCH_URL = 'http://www.cantonese.sheik.co.uk/scripts/wordsearch.php?level=0'

def cantodict_search_char(search_string, verbosity):
    query = { "SEARCHTYPE":CANTODICT_CHINESE_CHAR_SEARCH, "TEXT":search_string }
    x = requests.post(CANTODICT_SEARCH_URL, data=query)

    # Cantodict is returning a page with a refresh meta such as :
    # meta http-equiv="refresh" content="1;url=http://www.cantonese.sheik.co.uk/dictionary/characters/93/">
    m = re.search('http-equiv="refresh" content="1;url=(.+)"', x.text)
    if not m:
        print("No results")
        return

    character_page_url = m.group(1)

    if verbosity > 0:
        print(character_page_url)

    x = requests.get(character_page_url)
    raw_table = None
    # Result is badly formatted XML, we need to use BeautifulSoup first
    # and look for a <table> of class charactercard
    soup = BeautifulSoup(x.text, 'html.parser')
    for table in soup.find_all('table'):
        if "class" not in table.attrs or "charactercard" not in table["class"]:
            continue
        raw_table = table.prettify()
        break

    if not raw_table:
        print("No results")
        return

    table = ET.fromstring(raw_table)
    
    # sad, we keep a context boolean to correctly display classifier characters
    in_classifier_context = False
    classifiers = ""

    for tr in table.findall('./tr'):
        jyutping = tr.find(".//span[@class='cardjyutping']")
        if jyutping is None:
            continue

        jyutping_res = ""
        for o in jyutping.itertext():
            res = o.strip()
            if res == "":
                continue
            jyutping_res += res
        print("JyutPing:", jyutping_res)

    for tr in table.findall('./tr'):
        meaning = tr.find(".//td[@class='wordmeaning']")
        if meaning is None:
            continue

        for o in meaning.itertext():
            # word definitions
            res = o.strip()
            if res == "":
                continue
            if re.match("\[[0-9]+\]", res):
                print(res)
            elif re.match("Radical:", res):
                continue
            elif re.match("\(#[0-9]+\)", res):
                print("Radical:", res)
            elif re.match("Classifier", res):
                in_classifier_context = True
                classifiers += res + " "
            elif re.match("Stroke count", res):
                print(res)
            else:
                if res == ")":
                    in_classifier_context = False
                if in_classifier_context:
                    classifiers += res
                else:
                    print(res)
        print(classifiers)


def cantodict_generic_search(search_string, search_type, verbosity):
    query = { "SEARCHTYPE":search_type, "TEXT":search_string }
    x = requests.post(CANTODICT_SEARCH_URL, data=query)

    # Result is badly formatted XML, we need to use BeautifulSoup first
    soup = BeautifulSoup(x.text, 'html.parser')

    # English search has two tables : characters and words
    # JyutPing search has only one.
    for table in soup.find_all('table'):
        # Not a typo, the jyutping search returns a string multiple spaces
        s = re.search("Found ([0-9]+) (character|word)? entries for.*", table.text)
        if not s:
            continue

        if s.group(2):
            entries_count = f"Found {s.group(1)} {s.group(2)} entries"
        else:
            entries_count = f"Found {s.group(1)} jyutping entries"

        # Word table is badly formatted in "search english" mode 
        # There are two levels of unclosed <tr>
        targets = "./tr"
        if search_type == CANTODICT_ENGLISH_SEARCH:
            targets = "./tr/tr/tr"

        # in english search, words table doesn't repeat search url
        try:
            search_url = re.search('Link to this search: (.*)', table.text).group(1)
        except:
            search_url = None

        # Printing some infos
        if search_url and verbosity > 0:
            print(search_url)
            print(entries_count)

        table_ = ET.fromstring(table.prettify())

        for tr in table_.findall(targets):
            try:
                url = tr.find("./td[1]/a").attrib['href']
            except:
                continue

            word = ""
            for o in tr.find(".//span[@class='chinesemed']").itertext():
                word += o.strip()

            jyutping = ""
            for o in tr.find(".//span[@class='listjyutping']").itertext():
                jyutping += o.strip()

            meaning = ""
            for o in tr.find("./td[5]").itertext():
                meaning += o.strip()

            res = f"{word}\t{jyutping}\t{meaning}"
            if verbosity > 1:
                res = f"{url}\t{word}\t{jyutping}\t{meaning}"
            print(res)


def cantodict_search_chinese(search_string, verbosity):
    if len(search_string) == 1:
        cantodict_search_char(search_string, verbosity)
    else:
        cantodict_generic_search(search_string, CANTODICT_CHINESE_WORD_SEARCH, verbosity)

def cantodict_search_english(search_string, verbosity):
    cantodict_generic_search(search_string, CANTODICT_ENGLISH_SEARCH, verbosity)


def cantodict_search_jyutping(search_string, verbosity):
    cantodict_generic_search(search_string, CANTODICT_JYUTPING_SEARCH, verbosity)


def print_help():
    print("USAGE : canto.py -h -v -j jyuping_string -c chinese_characters -e english_word")
    print("NOTE : when using chinese character search, place the verbose -v flag before the search flag")


def main():
    cmd_line_args = sys.argv[1:]
    opts, args = getopt.getopt(cmd_line_args,"hc:j:e:vV",["english=","chinese=","jyutping=","verbose","verbose-more"])
    verbosity = 0
    search_string = None
    search_function = None
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-c", "--chinese"):
            search_function = cantodict_search_chinese
            search_string = arg
        elif opt in ("-j", "--jyutping"):
            search_function = cantodict_search_jyutping
            search_string = arg
        elif opt in ("-e", "--english"):
            search_function = cantodict_search_english
            search_string = arg
        elif opt in ("-v", "--verbose"):
            verbosity = 1
        elif opt in ("-V", "--verbose-more"):
            verbosity = 2

    if search_string is None or search_function is None:
        print_help()
        sys.exit()

    search_function(search_string, verbosity)

if __name__ == "__main__":
    main()
